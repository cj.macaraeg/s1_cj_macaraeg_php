<?php

// Variables
// Variables are defined using the dollar ($) notation before the name of the variable.

	$name = 'John Smith';
	$email = 'johnsmith@gmail.com';

// Constants

	define('PI', 3.1416);

// Data Types

	// Strings
		$state = 'New York';
		$country = 'USA';
		// $address = $state. ',' .$country; //concatenation via dot sign
		$address = "$state, $country"; //concatenation via double quotes

	// Integers
		$age = 31;
		$headcount = 26;

	// Decimal
		$grade = 98.2;
		$distanceInKilometer = 1234.56;

	// Boolean
		$hasTravelledAbroad = false;
		$haveSymptoms = true;

	// Null
		$boyfriend = null;
		$girlfriend = null;

	// Arrays
		$grades = array(90.7, 87, 98, 93);

	// Objects
		$gradesObj = (object)[
			'firstGrading' => 98.7,
			'secondGrading' => 92.1,
			'thirdGrading' => 90,
			'fourthGrading' => 95,
		];

		$personObj = (object)[
			'fullname' => 'John Doe',
			'isMarried' => false,
			'age' => 35,
			'address' => (object)[
				'state' => 'New York',
				'country' => 'USA',
				]
		];

// Operators

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;

// Functions

function getFullName($firstName = "User", $middleInitial, $lastName){
	return "$lastName, $firstName, $middleInitial";
}

// getFullName(); not here


function determineTyphoonIntensity($windSpeed) {
	if ($windSpeed < 30) {
		return 'Not a typhoon yet';
	} else if ($windSpeed <= 61) {
		return 'Tropical depression detected';
	} else if ($windSpeed >= 62 && $windSpeed <= 88){
		return 'Tropical Storm detected';
	} else if ($windSpeed >= 89 && $windSpeed <= 117){
		return 'Severe tropical storm detected';
	} else {
		return 'Typhoon detected';
	}

	}

// Conditional (ternary) Operator

function isUnderAge($age) {
	return ($age < 18) ? true : false;
}

//Switch Statement

function determineComputerUser($computerNumber){
	switch ($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2: 
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meier';
			break;
		case 4:
			return 'Onel de Guzman';
			break;
		case 5:
			return 'Christian Salvador';
			break;
		default: 
			return $computerNumber. " is out of the bounds.";
	}
};


//Try-Catch-Finally Statement

function greeting($str) {
	try {
		// Attempt to execute a code.
		if(gettype($str) == "string"){
			echo $str;
		} else {
			throw new Exception("Oppss!");
		}
	}

	catch (Exception $e) {
		// Catch erros within "try"
		echo $e->getMessage();

	}

	finally {
		// continue execution of code regardless of success and failure of code execution
		echo "I did it again!";
	}
}

// Activity 1

function getFullAddress($country, $city, $province, $specificAddress) {
  return $specificAddress . ', ' . $city . ', ' . $province . ', ' . $country;
}

// Activity 2

function getLetterGrade($grade) {
  if ($grade >= 98 && $grade <= 100) {
    return $grade. ' is equivalent to A+';
  } elseif ($grade >= 95 && $grade <= 97) {
    return $grade. ' is equivalent to A';
  } elseif ($grade >= 92 && $grade <= 94) {
    return $grade. ' is equivalent to A-';
  } elseif ($grade >= 89 && $grade <= 91) {
    return $grade. ' is equivalent to B+';
  } elseif ($grade >= 86 && $grade <= 88) {
    return $grade. ' is equivalent to B';
  } elseif ($grade >= 83 && $grade <= 85) {
    return $grade. ' is equivalent to B-';
  } elseif ($grade >= 80 && $grade <= 82) {
    return $grade. ' is equivalent to C+';
  } elseif ($grade >= 77 && $grade <= 79) {
    return $grade. ' is equivalent to C';
  } elseif ($grade >= 75 && $grade <= 76) {
    return $grade. ' is equivalent to C-';
  } else {
    return $grade. ' is equivalent to F';
  }
}


























?>