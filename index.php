<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>s01: PHP Basics and Selections</title>
	</head>
	<body>
		<!-- <h1>Hellow Wurld?</h1> -->

		<h1>Echoing Values</h1>
		<!-- Variables can be used to output data in double quotes while single quotes do not. -->

		<p>
			<?php echo 'Good day $name! Your given email is $email' ?>
		</p>

		<p>
			<?php echo "Good day $name! Your given email is $email" ?>
		</p>

		<p>
			<?php echo PI ?>
		</p>

		<h1>Data Types</h1>

		<p>
			<?php echo "$address"?>
			<?php echo "$age"?>
		</p>

	<!-- To output the value of an object property, the arrow  -->

		<p>
			<?php echo $gradesObj->firstGrading; ?>
			<?php echo $personObj->address->state; ?>
		</p>

		<p>
			<?php echo $girlfriend; ?>
		</p>


<!-- Normal echoing of null and boolean var will not make it visible to the web page -->
		<p>
			<?php echo gettype($hasTravelledAbroad); ?>
		</p>

		<p>
			<?php echo var_dump($hasTravelledAbroad); ?>
		</p>

		<p>
			<?php echo var_dump($girlfriend); ?>
		</p>

		<p>
			<?php echo $grades[2]; ?>
		</p>

		<h1>Operators</h1>

		<h2>Arithmetic Operators</h2>

		<p>
			Sum: <?php echo $x + $y ?> 
		</p>

		<h2>Equality Operator</h2>

		<p>Loose Equality: <?php echo var_dump($x == '1342.14'); ?></p>
		<p>Loose Equality: <?php echo var_dump($x != '1342.14'); ?></p>

		<h2>Greater/lesser Operators</h2>
		<p>is greater: <?php echo var_dump($x > $y); ?></p>

		<h2>Logical Operators</h2>

		<p>Are All requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>

		<p>Are All requirements Met: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>

		<p>Are All requirements Met: <?php echo var_dump($isLegalAge ==  !$isRegistered); ?></p>

		<h1> Function </h1>
		<!-- Syntax -->

		<p>Full Name: <?php echo getFullName('John', 'P', 'Doe'); ?></p>


		<!-- Selection Control Structures -->
		<h2>If-elseif-else</h2>

		<p><?php echo determineTyphoonIntensity(35); ?></p>

		<h2>Ternary</h2>
		<p>78: <?php echo var_dump(isUnderAge(78)); ?></p>

		<h2>Switch</h2>

		<p><?php echo determineComputerUser(1); ?></p>

		<p><?php echo greeting("HAHA "); ?></p>


		<!-- Activity 1 -->

		<h1> THIS IS ACTIVITY #1</h1>


		<p><?php echo getFullAddress("Blk 221 Phase 7", "Bahay Pare", "City of Meycauayan", "Bulacan"); ?></p>

		<h1> THIS IS ACTIVITY #2</h1>

		<p><?php echo getLetterGrade(87); ?></p>
		<p><?php echo getLetterGrade(94); ?></p>
		<p><?php echo getLetterGrade(74); ?></p>












	</body>
</html>

